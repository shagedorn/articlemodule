//
//  ArticleViewController.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <InfrastructureModule/Module.h>

/**
 *  This controller displays all articles in a table view
 *  on the left and details of the currently selected cell on the right.
 */
@interface ArticleViewController : UIViewController <Module, UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>

@end
