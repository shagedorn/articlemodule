//
//  Article.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import <InfrastructureModule/ServerEntity.h>

@class ArticleColour;

/**
 *  Represents one article.
 */
@interface Article : NSManagedObject <ServerEntity>

/**
 *  A unique ID.
 */
@property (nonatomic, retain) NSNumber * articleId;

/**
 *  A descriptive name.
 */
@property (nonatomic, retain) NSString * name;

/**
 *  The size.
 */
@property (nonatomic, retain) NSNumber * size;

/**
 *  The article's colour.
 */
@property (nonatomic, retain) ArticleColour *colour;

@end


