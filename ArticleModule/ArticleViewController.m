//
//  ArticleViewController.m
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "ArticleViewController.h"
#import "Article.h"
#import "ArticleColour.h"

#import <InfrastructureModule/CoreDataAppDelegate.h>
#import <InfrastructureModule/CoreDataHelperClass.h>
#import <InfrastructureModule/BundleResourceHelperClass.h>

#import <AFNetworking.h>

#pragma mark - Private Interface

@interface ArticleViewController ()

#pragma mark - IB Outlets

@property (weak, nonatomic) IBOutlet UIButton *loadArticlesButton;
@property (weak, nonatomic) IBOutlet UIView *loadArticlesContainerView;
@property (weak, nonatomic) IBOutlet UIProgressView *loadArticlesProgressBar;
@property (weak, nonatomic) IBOutlet UITableView *masterTableView;
@property (weak, nonatomic) IBOutlet UIView *articleDetailsContainerView;
@property (weak, nonatomic) IBOutlet UILabel *detailID;
@property (weak, nonatomic) IBOutlet UILabel *detailSize;
@property (weak, nonatomic) IBOutlet UILabel *detailColour;
@property (weak, nonatomic) IBOutlet UILabel *detailColourId;
@property (weak, nonatomic) IBOutlet UILabel *detailTitle;

@property (weak, nonatomic) IBOutlet UILabel *detailIDStatic;
@property (weak, nonatomic) IBOutlet UILabel *detailSizeStatic;
@property (weak, nonatomic) IBOutlet UILabel *detailColourStatic;
@property (weak, nonatomic) IBOutlet UILabel *detailColourIdStatic;

@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *allDetailTextLabels;

#pragma mark - Private Properties

@property (nonatomic, copy) UpdateBlock updateBlock;
@property (nonatomic, copy) CompletionBlock completionBlock;
@property (nonatomic, strong) NSOperationQueue *articleLoadingQueue;
@property (nonatomic, strong) NSFetchedResultsController *resultsController;
@property (nonatomic, strong) NSDictionary *moduleInfo;
@property (nonatomic, readonly) NSString *bundleName;

#pragma mark - Private methods

///==========================================
/** User actions */
///==========================================
/**
 *  Called when the user clicks the 'x' button.
 *
 *  It releases the view and
 *  calls the closeModuleBlock.
 *
 *  @param sender The close button.
 */
- (void) close:(id)sender;

/**
 *  Triggers loading article data.
 *
 *  @param sender The load button.
 */
- (IBAction)loadArticles:(id)sender;

///==========================================
/** Data loading */
///==========================================
/**
 *  Loading data was successfull.
 *
 *  @param serverResponse The data coming from the server.
 *  Assumed to be text.
 */
- (void) dataHasBeenLoaded:(id)serverResponse;

/**
 *  Loading data from the server failed. The error will be logged.
 *
 *  @param error An error description.
 */
- (void) dataLoadingFailed:(NSError*)error;

@end

#pragma mark - Implementation

@implementation ArticleViewController {}

#pragma mark - Accessors

- (NSString *)bundleName {
    return [self.moduleInfo objectForKey:@"BundleName"];
}

- (NSOperationQueue*) articleLoadingQueue {
    if (!_articleLoadingQueue) {
        _articleLoadingQueue = [[NSOperationQueue alloc] init];
    }
    return _articleLoadingQueue;
}

#pragma mark - Module Protocol

@synthesize closeModuleBlock = _closeModuleBlock;

- (UIImage *)modulePreviewImage {
    NSString *path = [BundleResourceHelperClass pathForResource:@"module_preview_article"
                                                         ofType:@"png"
                                                       inModule:self.bundleName];
    return [UIImage imageWithContentsOfFile:path];
}

- (UIViewController *)moduleLaunchController {
    return self;
}

- (NSArray *)moduleDependencies {
    return [self.moduleInfo objectForKey:@"ModuleDependencies"];
}

- (void)moduleLoadData:(NSManagedObjectContext *)context
            usingBlockForProgress:(UpdateBlock)updateBlock
                    andCompletion:(CompletionBlock)completionBlock {

    self.loadArticlesProgressBar.progress = 0.0;

    // these are only passed in when called from outside
    self.updateBlock = updateBlock;
    self.completionBlock = completionBlock;

    // prevent loading data twice
    self.loadArticlesButton.enabled = NO;

    // prepare loading data
    NSString *serverPath = [[NSBundle mainBundle].infoDictionary objectForKey:@"ServerURL"];
    NSString *articlePLISTPath = [BundleResourceHelperClass pathForResource:@"ArticleServerInfo"
                                                           ofType:@"plist"
                                                         inModule:self.bundleName];
    NSDictionary *articlePLIST = [NSDictionary dictionaryWithContentsOfFile:articlePLISTPath];
    NSString *filename = [articlePLIST objectForKey:@"filename"];
    NSString *extension = [articlePLIST objectForKey:@"format"];
    NSURL *url = [NSURL URLWithString:[[serverPath stringByAppendingPathComponent:filename]
                                       stringByAppendingPathExtension:extension]];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url
                                                  cachePolicy:NSURLCacheStorageNotAllowed
                                              timeoutInterval:30];
    AFHTTPRequestOperation *dataLoadingOperation = [[AFHTTPRequestOperation alloc] initWithRequest:request];

    // intercept progress notifications
    [dataLoadingOperation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead) {
        // update local progress bar
        float localRatio = (float)totalBytesRead/(float)totalBytesExpectedToRead;
        self.loadArticlesProgressBar.progress = localRatio;

        // update global progress bar
        if (self.updateBlock) {
            NSNumber *ratio = [NSNumber numberWithFloat:localRatio];
            self.updateBlock(ratio, self);
        }
    }];

    // intercept loading has finished notification
    [dataLoadingOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        // local processing
        [self dataHasBeenLoaded:responseObject];

        // global progress bar update
        if (self.completionBlock) {
            self.completionBlock(nil, self);
        }
        // don't reuse it for further operations
        self.completionBlock = nil;
        self.updateBlock = nil;
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // local error notification
        [self dataLoadingFailed:error];

        // global error notification
        if (self.completionBlock) {
            self.completionBlock(error, self);
        }
        // don't reuse it for further operations
        self.completionBlock = nil;
        self.updateBlock = nil;
        
    }];

    // start loading
    [self.articleLoadingQueue addOperation:dataLoadingOperation];
}

- (NSString *)moduleDescription {
    return [self.moduleInfo objectForKey:@"ModuleDisplayName"];
}

#pragma mark - Lifecycle

- (id)init {
    NSBundle *myBundle = [BundleResourceHelperClass bundleForModule:@"ArticleModuleBundle"];
    self = [super initWithNibName:@"ArticleViewController" bundle:myBundle];
    if (self) {
        // Load module information
        NSString *plistPath = [myBundle pathForResource:@"ModuleDescription" ofType:@"plist"];
        self.moduleInfo = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    }

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib
    self.masterTableView.layer.cornerRadius = 10.0f;
    self.masterTableView.layer.masksToBounds = YES;

    // Set title
    self.navigationItem.title = [self moduleDescription];

    // Set close button
    NSString *closeButtonPath = [BundleResourceHelperClass pathForResource:@"close_button"
                                                                    ofType:@"png"
                                                                  inModule:@"InfrastructureModuleBundle"];
    NSString *closeButtonPressedPath = [BundleResourceHelperClass pathForResource:@"close_button_pressed"
                                                                           ofType:@"png"
                                                                         inModule:@"InfrastructureModuleBundle"];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage alloc] init]
                                                                             style:UIBarButtonItemStyleBordered
                                                                            target:self
                                                                            action:@selector(close:)];
    [self.navigationItem.leftBarButtonItem setBackgroundImage:[UIImage imageWithContentsOfFile:closeButtonPath]
                                                     forState:UIControlStateNormal
                                                   barMetrics:UIBarMetricsDefault];
    [self.navigationItem.leftBarButtonItem setBackgroundImage:[UIImage imageWithContentsOfFile:closeButtonPressedPath]
                                                     forState:UIControlStateHighlighted
                                                   barMetrics:UIBarMetricsDefault];
    
    // Set localised strings
    [self.loadArticlesButton setTitle:NSLocalizedString(@"Article.ArticleViewController.LoadArticles", @"Load articles button")
                             forState:UIControlStateNormal];
    self.detailColourIdStatic.text = NSLocalizedString(@"Article.ArticleViewController.ColourID", @"Colour ID (short)");
    self.detailIDStatic.text = NSLocalizedString(@"Article.ArticleViewController.ID", @"Article ID (short)");
    self.detailSizeStatic.text = NSLocalizedString(@"Article.ArticleViewController.Size", @"Article size (short)");
    self.detailColourStatic.text = NSLocalizedString(@"Article.ArticleViewController.Colour", @"Article Colour (short)");

    // Style load button
    NSString *buttonBackgroundPath = [BundleResourceHelperClass pathForResource:@"button"
                                                                         ofType:@"png"
                                                                       inModule:@"InfrastructureModuleBundle"];
    UIImage *resizableImage = [UIImage imageWithContentsOfFile:buttonBackgroundPath];
    resizableImage = [resizableImage resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)
                                                    resizingMode:UIImageResizingModeStretch];

    buttonBackgroundPath = [BundleResourceHelperClass pathForResource:@"button_pressed"
                                                               ofType:@"png"
                                                             inModule:@"InfrastructureModuleBundle"];
    UIImage *resizablePressedImage = [UIImage imageWithContentsOfFile:buttonBackgroundPath];
    resizablePressedImage = [resizablePressedImage resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)
                                                                  resizingMode:UIImageResizingModeStretch];
    [self.loadArticlesButton setBackgroundImage:resizableImage forState:UIControlStateNormal];
    [self.loadArticlesButton setBackgroundImage:resizablePressedImage forState:UIControlStateHighlighted];

    // Init load articles container view
    self.loadArticlesContainerView.layer.cornerRadius = 10.0f;
    self.articleDetailsContainerView.layer.cornerRadius = 10.0f;
    for (UILabel *label in self.allDetailTextLabels) {
        label.alpha = 0.0;
    }

    // Initialise data
    NSManagedObjectContext *context = [(id<CoreDataAppDelegate>)[UIApplication sharedApplication].delegate managedObjectContext];
    NSSortDescriptor *sorting = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([Article class])];
    request.sortDescriptors = @[sorting];
    self.resultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request
                                                                 managedObjectContext:context
                                                                   sectionNameKeyPath:nil
                                                                            cacheName:nil];
    self.resultsController.delegate = self;

    NSError *fetchError;
    BOOL success = [self.resultsController performFetch:&fetchError];
    if (fetchError || !success) {
        DLog(@"Error fetching Articles: %@", fetchError);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc {
    [self.articleLoadingQueue cancelAllOperations];
}

#pragma mark - IB Actions

- (IBAction)loadArticles:(id)sender {
    self.loadArticlesProgressBar.alpha = 1.0;
    id<CoreDataAppDelegate> appDelegate = (id<CoreDataAppDelegate>)[UIApplication sharedApplication].delegate;
    [self moduleLoadData:[appDelegate managedObjectContext]
        usingBlockForProgress:nil
                andCompletion:nil];
}

#pragma mark - Other user actions

- (void) close:(id)sender {
    self.closeModuleBlock(^{
        // unload view when module is loaded but not showing
        self.view = nil;
    });
}

#pragma mark - Process incoming data

- (void) dataHasBeenLoaded:(id)serverResponse {
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.loadArticlesProgressBar.alpha = 0.5;
                         self.loadArticlesButton.enabled = YES;
                     }
                     completion:^(BOOL finished) {}];

    @try {
        // expecting XML/plist string
        NSString *string = [[NSString alloc] initWithData:serverResponse encoding:NSUTF8StringEncoding];
        NSDictionary *plist = [string propertyList];

        NSArray *colours = [plist objectForKey:@"colours"];
        NSArray *articles = [plist objectForKey:@"articles"];

        NSManagedObjectContext *context = [(id<CoreDataAppDelegate>)[UIApplication sharedApplication].delegate
                                           managedObjectContext];

        NSError *error;
        // ArticleColours first
        error = [CoreDataHelperClass saveOrUpdateAllServerEntitiesOfType:[ArticleColour class]
                                        withPrimaryDictionaryKey:@"id"
                                         andPrimaryAttributeName:@"articleColourId"
                                                      fromSource:colours
                                                            into:context];
        if (error) {
            DLog(@"ArticleColours Error: %@", error);
            return;
        }

        // Articles next
        error = [CoreDataHelperClass saveOrUpdateAllServerEntitiesOfType:[Article class]
                                        withPrimaryDictionaryKey:@"id"
                                         andPrimaryAttributeName:@"articleId"
                                                      fromSource:articles
                                                            into:context];
        if (error) {
            DLog(@"Articles Error: %@", error);
            return;
        }
    }
    @catch (NSException *exception) {
        DLog(@"WARNING: Server response could not be parsed. (%@)", exception);
    }
}

- (void) dataLoadingFailed:(NSError*)error {
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.loadArticlesProgressBar.alpha = 0.5;
                         self.loadArticlesButton.enabled = YES;
                     }
                     completion:^(BOOL finished) {}];
    DLog(@"Error loading articles: %@", error);
}

#pragma mark - Table View Data Source

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    return NSLocalizedString(@"Article.ArticleViewController.TableFooter", @"Master Table Footer");
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.resultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.resultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *cellIdentifier = @"masterArticleCell";
    UITableViewCell *cell = [self.masterTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:cellIdentifier];
    }

    Article *article = [self.resultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ (%@)", article.name, article.size];
    NSString *idString = NSLocalizedString(@"Article.ArticleViewController.ID", @"Article ID (short)");
    NSString *colourString = NSLocalizedString(@"Article.ArticleViewController.Colour", @"Article Colour (short)");
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@: %@ | %@: %@", idString, article.articleId, colourString, article.colour.name];

    return cell;
}

#pragma mark - Table View Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Article *selectedArticle = [self.resultsController objectAtIndexPath:indexPath];
    self.navigationItem.title = [NSString stringWithFormat:@"%@: %@", [self moduleDescription], selectedArticle.name];

    // Update detail view
    float duration = 0.2;
    [UIView animateWithDuration:duration
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         for (UILabel *label in self.allDetailTextLabels) {
                             label.alpha = 0.0;
                         }
                     }
                     completion:^(BOOL finished) {
                         // actually update the labels
                         self.detailTitle.text = selectedArticle.name;
                         self.detailID.text = selectedArticle.articleId.stringValue;
                         self.detailSize.text = selectedArticle.size.stringValue;
                         self.detailColour.text = selectedArticle.colour.name;
                         self.detailColourId.text = selectedArticle.colour.articleColourId.stringValue;

                         [UIView animateWithDuration:duration
                                               delay:duration
                                             options:UIViewAnimationOptionBeginFromCurrentState
                                          animations:^{
                                              for (UILabel *label in self.allDetailTextLabels) {
                                                  label.alpha = 1.0;
                                              }
                                          }
                                          completion:^(BOOL finished) {
                                              // Nothing to do
                                          }];
                     }];
    
}

#pragma mark - FetchedResultsController Delegate

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.masterTableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {

    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.masterTableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationAutomatic];
            break;

        case NSFetchedResultsChangeDelete:
            [self.masterTableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex]
                          withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {

    UITableView *tableView = self.masterTableView;

    switch(type) {

        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationAutomatic];
            break;

        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationAutomatic];
            break;

        case NSFetchedResultsChangeUpdate:
            [self.masterTableView reloadRowsAtIndexPaths:@[indexPath]
                                        withRowAnimation:UITableViewRowAnimationAutomatic];
            break;

        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationAutomatic];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.masterTableView endUpdates];
}

@end
