//
//  Article.m
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import "Article.h"
#import "ArticleColour.h"

@implementation Article

#pragma mark - Core Data Properties

@dynamic articleId;
@dynamic name;
@dynamic size;
@dynamic colour;

#pragma mark - ServerEntity Protocol

- (void)updateWithInformation:(NSDictionary *)infoDict {
    BOOL hasChanged = NO;

    // Set ID
    if (!self.articleId) {
        // Do not overwrite existing IDs
        NSString *initialId = [infoDict objectForKey:@"id"];
        self.articleId = [[NSNumber alloc] initWithInteger:[initialId integerValue]];
        DLog(@"Article '%@' has been initialised.", self.articleId);
    }

    // Set name
    NSString *newName = [infoDict objectForKey:@"name"];
    if (newName && ![newName isEqualToString:self.name]) {
        self.name = newName;
        hasChanged = YES;
    }

    if (hasChanged) {
        DLog(@"Article '%@' has changed.", self.name);
    }

    // Set size
    NSNumber *size = [infoDict objectForKey:@"size"];
    if (size && (self.size.floatValue != size.floatValue)) {
        self.size = size;
        hasChanged = YES;
    }

    // Set colour
    NSString *colourIdString = [infoDict objectForKey:@"colour"];
    if (colourIdString) {
        NSInteger colourId = [colourIdString integerValue];
        ArticleColour *myColour = [ArticleColour colourWithID:colourId inContext:self.managedObjectContext];
        if (myColour && (myColour.articleColourId != self.colour.articleColourId)) {
            self.colour = myColour;
            hasChanged = YES;
        }
    }

    if (hasChanged) {
        DLog(@"Article '%@' has changed.", self.name);
    }
    // orders is never set directly
}

@end
