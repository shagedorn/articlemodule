//
//  ArticleColour.h
//  Modular ERP App
//
//  Created by Sebastian Hagedorn on 6/11/12.
//  Copyright (c) 2012 TU Dresden/SALT Solutions GmbH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import <InfrastructureModule/ServerEntity.h>

@class Article;

/**
 *  Represents the colour of an article.
 */
@interface ArticleColour : NSManagedObject <ServerEntity>

/**
 *  A unique ID.
 */
@property (nonatomic, retain) NSNumber * articleColourId;

/**
 *  The colour's name.
 */
@property (nonatomic, retain) NSString * name;

/**
 *  All articles with this colour.
 *
 *  The property is only set indirectly via
 *  its inverse relation.
 */
@property (nonatomic, retain) NSSet *articles;

/**
 *  Tries to find an ArticleColour for the given ID.
 *
 *  In case many objects are found, the last one is returned.
 *  However, as the ID is supposed to be unique, this case
 *  should never happen.
 *
 *  @param colourId The article colour's ID.
 *  @param context The managed object context which will be used for searching.
 *
 *  @return The colour object, if found. Else nil.
 */
+ (ArticleColour*) colourWithID:(NSInteger)colourId inContext:(NSManagedObjectContext*)context;

@end


@interface ArticleColour (CoreDataGeneratedAccessors)

- (void)addArticlesObject:(Article *)value;
- (void)removeArticlesObject:(Article *)value;
- (void)addArticles:(NSSet *)values;
- (void)removeArticles:(NSSet *)values;

@end
